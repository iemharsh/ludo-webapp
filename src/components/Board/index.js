/* eslint-disable no-undef */
import React from "react";
import { StyledBoard } from "./style";

function Board(props) {
  const boxSize = 30;
  const playersColor = [
    "rgb(72, 158, 80)",
    "rgb(217, 56, 48)",
    "rgb(82, 172, 244)",
    "rgb(250, 222, 78)",
  ];
  return (
    <StyledBoard
      className="board"
      boxSize={boxSize}
      playersColor={playersColor}
    >
      {playersColor.map((c, idx) => (
        <div key={idx} className={`player-${idx}-den`}></div>
      ))}
      <div className="center"></div>
    </StyledBoard>
  );
}
export default Board;
