import styled from "styled-components";
import { css } from "styled-components";

export const StyledBoard = styled.div`
  width: ${(props) => `${props.boxSize * 15}px`};
  height: ${(props) => `${props.boxSize * 15}px`};
  position: relative;
  border: 1px solid red;
  ${(props) =>
    props.playersColor.map(
      (c, idx) => css`
        & .player-${idx}-den {
          position: absolute;
          width: ${(prosp) => props.boxSize * 6}px;
          height: ${(prosp) => props.boxSize * 6}px;
          box-sizing: border-box;
          border: ${(props) => props.boxSize}px solid
            ${(props) => props.playersColor[idx]};
        }
      `
    )}
  & .player-1-den {
    top: ${(props) => props.boxSize * 6 * (3 / 2)}px;
  }
  & .player-2-den {
    right: 0;
    bottom: 0;
  }
  & .player-3-den {
    right: 0;
  }
  & .center {
    position: absolute;
    top: ${(props) => `${props.boxSize * 6}px`};
    left: ${(props) => `${props.boxSize * 6}px`};
    background: black;
    border-top: ${(props) => `${props.boxSize * (3 / 2)}px`} solid
      ${(props) => props.playersColor[0]};
    border-left: ${(props) => `${props.boxSize * (3 / 2)}px`} solid
      ${(props) => props.playersColor[1]};
    border-bottom: ${(props) => `${props.boxSize * (3 / 2)}px`} solid
      ${(props) => props.playersColor[2]};
    border-right: ${(props) => `${props.boxSize * (3 / 2)}px`} solid
      ${(props) => props.playersColor[3]};
    width: 0px;
    height: 0px;
  }
`;
